import pygame


class Marciano(pygame.sprite.Sprite):
    
    #ATRIBUTOS
    spritesheet_x=None
    frames_derecha={}
    frames_izquierda={}
    frame=0

    def __init__(self):
        
        super().__init__()

        self.spritesheet_x = pygame.image.load("assets/spritesheets/marciano.png").convert()

        self.spritesheet_x.set_clip(pygame.Rect(0,0, 32, 48))
        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())

        self.frames_derecha = { 0:(160, 0, 32,48), 
                               1:(192, 0, 32,48), 
                               2:(224, 0, 32,48), 
                               3:(256, 0, 32,48) }
        
        self.frames_izquierda = { 0:(0, 0, 32, 48),
                                  1: (32, 0, 32, 48),
                                  2: (64, 0, 32, 48),
                                  3: (96, 0, 32, 48)
                                 }
        
        self.rect = self.image.get_rect()


    def devolver_frame(self, animacion):
       
        self.frame += 1
        if self.frame >= len(animacion):
            self.frame = 0
        
        return animacion[self.frame]


    def clip(self, animacion):
        self.spritesheet_x.set_clip(pygame.Rect(self.devolver_frame(animacion)))
     
    

    def update(self, direccion):
        
        self.image = self.spritesheet_x.subsurface(self.spritesheet_x.get_clip())
        
        if direccion == "derecha":
            
            self.clip(self.frames_derecha)

            self.rect.x += 10
    
    def administrar_eventos(self, evento):
        if evento.type == pygame.KEYDOWN:
            if evento.key == pygame.K_RIGHT:
                #self.clip(self.frames_derecha)
                self.update("derecha")