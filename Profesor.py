from Persona import Persona
from Departamento import Departamento

class Profesor(Persona, Departamento):
    sueldo = 0

    def set_sueldo(self, sueldo):
        self.sueldo = sueldo

    def get_sueldo(self):
        return self.sueldo