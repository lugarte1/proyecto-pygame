from clases.Personaje1 import Marciano
from clases.Personaje2 import Goku 
import pygame
import sys

class Juego:
    pygame.init()

    ventana = pygame.display.set_mode((500,479))

    reloj = pygame.time.Clock()

    marciano = Marciano()

    goku = Goku()

    def jugar(self):
        

        while True:     #Bucle de "Juego"
            ''' Esto significa que se van realizan 30
                actualizaciones del juego por segundo.
                Es necesario hacerlo en cada iteración
                por que si no se reinicia
            '''

            self.ventana.fill((18, 200, 30))

            for event in pygame.event.get():    #Cuando ocurre un evento...
                
                #print("EVENTO: ", pygame.key.get_pressed())
                if event.type == pygame.QUIT:   #Si el evento es cerrar la ventana
                    pygame.quit()        #Se cierra pygame
                    sys.exit()           #Se cierra el programa
        
                if event.type == pygame.KEYDOWN:
                    #print("Apreto tecla: ", event.key)
                    if event.key == pygame.K_RIGHT:
                        pass #marciano.update("derecha")

            if self.marciano.rect.x >=500:
                self.marciano.rect.x =0
            
            if self.marciano.rect.x < 0:
                self.marciano.rect.x = 500

            if self.marciano.rect.y < 479 - 53:
                self.marciano.rect.y +=5
            
            self.ventana.blit(self.marciano.image, self.marciano.rect)
            self.marciano.administrar_eventos(event)
            
            pygame.display.update()
            
            self.ventana.blit(self.goku.image, self.goku.rect)

            self.reloj.tick(20)

j = Juego()
j.jugar()